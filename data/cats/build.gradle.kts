plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.gmail.hejosadix.core.data.cats"
    compileSdk = 34
    buildFeatures {
        buildConfig = true
    }
    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            buildConfigField("String", "BASE_URL_IMAGE_CAT", "\"https://cataas.com/cat/\"")
        }
        debug {
            buildConfigField("String", "BASE_URL_IMAGE_CAT", "\"https://cataas.com/cat/\"")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(project(":core"))
    implementation(project(":core:network"))
    implementation(project(":core:database"))
    implementation(Libs.lifecycle)

    implementation(Libs.koin)

    implementation(Libs.moshi)

    implementation(Libs.paging)

    implementation(Libs.retrofit)
    implementation(Libs.retrofitMoshi)

    //Test
    testImplementation(Libs.junit)
    androidTestImplementation(Libs.junitExt)
    testImplementation(Libs.pagingTest)
    testImplementation(Libs.pagingTesting)
    testImplementation(Libs.coroutinesTest)
    testImplementation(Libs.mockito)
    testImplementation(Libs.mockitoKotlin)
}