package com.gmail.hejosadix.core.data.cats.utils

import com.gmail.hejosadix.core.data.cats.models.Cat


object FakeCats {
    fun generateFakeCats() = listOf(
        Cat(
            id = "1",
            owner = "null",
            tags = listOf("cute", "fluffy")
        ),
        Cat(
            id = "2",
            owner = "null",
            tags = listOf("cute", "fluffy")
        ),
        Cat(
            id = "3",
            owner = "null",
            tags = listOf("cute", "fluffy")
        ),
        Cat(
            id = "4",
            owner = "null",
            tags = listOf("cute", "fluffy")
        ),
        Cat(
            id = "5",
            owner = "null",
            tags = listOf("cute", "fluffy")
        ),
    )
}