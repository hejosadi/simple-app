package com.gmail.hejosadix.core.data.cats.repositories


import com.gmail.hejosadix.core.data.cats.BuildConfig
import com.gmail.hejosadix.core.data.cats.mappers.toDbCat
import com.gmail.hejosadix.core.data.cats.remote.datasources.CatsRemoteDataSource
import com.gmail.hejosadix.core.data.cats.utils.FakeCats
import com.gmail.hejosadix.core.database.cat.CatDao
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CatsRepositoryTest {
    @Mock
    private lateinit var remoteDataSource: CatsRemoteDataSource

    @Mock
    private lateinit var local: CatDao
    private lateinit var repository: CatsRepositoryImpl

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        repository = CatsRepositoryImpl(remoteDataSource, local)
    }

    @Test
    fun `test Fetch Cat`() = runTest {
        val data = FakeCats.generateFakeCats().first()

        Mockito.`when`(local.getCat(data.id)).thenReturn(
            flowOf(data.toDbCat())
        )
        val result = repository.fetchCat(data.id).first()

        assert(result == data)
    }

    @Test
    fun `test fetch url`() = runTest {
        val data = FakeCats.generateFakeCats().first()
        val result = repository.fetchCatImageUrl(data.id)
        val expected = "${BuildConfig.BASE_URL_IMAGE_CAT}${data.id}"
        assert(result == expected)
    }

}