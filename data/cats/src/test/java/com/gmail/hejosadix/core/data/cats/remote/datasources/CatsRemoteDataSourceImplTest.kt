package com.gmail.hejosadix.core.data.cats.remote.datasources

import com.gmail.hejosadix.core.data.cats.remote.api.CatsApi
import com.gmail.hejosadix.core.data.cats.utils.FakeCats
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

class CatsRemoteDataSourceImplTest {

    @Mock
    private lateinit var catsApi: CatsApi
    private lateinit var remoteDataSource: CatsRemoteDataSourceImpl

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        remoteDataSource = CatsRemoteDataSourceImpl(catsApi)
    }

    @Test
    fun `fetchCats returns data from API`() {
        val expectedData = FakeCats.generateFakeCats()
        val skip = 0
        val limit = expectedData.size
        runTest {
            whenever(catsApi.fetchCats(skip, limit)).thenReturn(expectedData)
        }
        val result = runBlocking { remoteDataSource.fetchCats(skip, limit) }
        assertEquals(expectedData, result)
    }
    @Test(expected = RuntimeException::class)
    fun `fetchCats throws error`() {
        val skip = 0
        val limit = 10
        runBlocking {
            whenever(catsApi.fetchCats(skip, limit)).thenThrow(RuntimeException("Fake error"))
        }
        runBlocking { remoteDataSource.fetchCats(skip, limit) }
    }

}