package com.gmail.hejosadix.core.data.cats.remote.api

import com.gmail.hejosadix.core.data.cats.models.Cat
import retrofit2.http.GET
import retrofit2.http.Query

internal interface CatsApi {
    @GET("cats")
    suspend fun fetchCats(
        @Query("skip") skip: Int,
        @Query("limit") limit: Int,
    ): List<Cat>
}