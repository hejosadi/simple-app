package com.gmail.hejosadix.core.data.cats.repositories

import androidx.paging.PagingData
import com.gmail.hejosadix.core.data.cats.models.Cat
import kotlinx.coroutines.flow.Flow

interface CatsRepository {
    fun fetchCats(): Flow<PagingData<Cat>>
    fun fetchCat(id: String): Flow<Cat>
    fun fetchCatImageUrl(id: String): String
}