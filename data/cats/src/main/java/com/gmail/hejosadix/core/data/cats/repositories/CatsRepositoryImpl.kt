package com.gmail.hejosadix.core.data.cats.repositories

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.gmail.hejosadix.core.common.Constants
import com.gmail.hejosadix.core.data.cats.BuildConfig
import com.gmail.hejosadix.core.data.cats.models.Cat
import com.gmail.hejosadix.core.data.cats.remote.CatsPagingRemoteMediator
import com.gmail.hejosadix.core.data.cats.remote.datasources.CatsRemoteDataSource
import com.gmail.hejosadix.core.database.cat.CatDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import com.gmail.hejosadix.core.data.cats.mappers.toCat


class CatsRepositoryImpl(
    private val remoteDataSource: CatsRemoteDataSource,
    private val local: CatDao
) : CatsRepository {
    @OptIn(ExperimentalPagingApi::class)
    override fun fetchCats(): Flow<PagingData<Cat>> {
        val imageUrl = fetchCatImageUrl("")
        return Pager(
            config = PagingConfig(
                Constants.defaultPageSize,
            ),
            remoteMediator = CatsPagingRemoteMediator(
                remote = remoteDataSource,
                local = local
            )
        ) {
            local.getAllCatPagingSource()
        }.flow.map { paddingData ->
            paddingData.map { dbCat ->
                dbCat.toCat(imageUrl.plus(dbCat.id))
            }
        }
    }


    override fun fetchCat(id: String) = local.getCat(id).map {
        it.toCat(
            fetchCatImageUrl(id)
        )
    }

    override fun fetchCatImageUrl(id: String): String {
        return BuildConfig.BASE_URL_IMAGE_CAT + id
    }

}