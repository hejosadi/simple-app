package com.gmail.hejosadix.core.data.cats.di

import com.gmail.hejosadix.core.data.cats.remote.api.CatsApi
import com.gmail.hejosadix.core.data.cats.remote.datasources.CatsRemoteDataSource
import com.gmail.hejosadix.core.data.cats.remote.datasources.CatsRemoteDataSourceImpl
import com.gmail.hejosadix.core.data.cats.repositories.CatsRepository
import com.gmail.hejosadix.core.data.cats.repositories.CatsRepositoryImpl
import org.koin.dsl.module
import retrofit2.Retrofit


val dataCatsModule = module {
    single<CatsApi> {
        val retrofit = get<Retrofit>()
        retrofit.create(CatsApi::class.java)
    }
    single<CatsRemoteDataSource> {
        CatsRemoteDataSourceImpl(get())
    }
    single<CatsRepository> { CatsRepositoryImpl(get(), get())  }
}

