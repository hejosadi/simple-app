package com.gmail.hejosadix.core.data.cats.remote

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.gmail.hejosadix.core.data.cats.mappers.toDbCat
import com.gmail.hejosadix.core.data.cats.remote.datasources.CatsRemoteDataSource
import com.gmail.hejosadix.core.database.cat.CatDao
import com.gmail.hejosadix.core.database.cat.DbCat
import retrofit2.HttpException
import java.io.IOException

@OptIn(ExperimentalPagingApi::class)
class CatsPagingRemoteMediator(
    private val local: CatDao, private val remote: CatsRemoteDataSource
) : RemoteMediator<Int, DbCat>() {
    override suspend fun initialize(): InitializeAction {
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    override suspend fun load(
        loadType: LoadType, state: PagingState<Int, DbCat>
    ): MediatorResult {

        val loadSize = when (loadType) {
            LoadType.REFRESH -> state.config.initialLoadSize
            else -> state.config.pageSize
        }
        val skip = when (loadType) {
            LoadType.REFRESH -> 0
            else -> local.count()
        }

        try {
            val data = remote.fetchCats(
                skip = skip, limit = loadSize
            )
            val items = data.map { it.toDbCat() }

            if (loadType == LoadType.REFRESH) {
                local.insertAndClearCache(items)
            } else {
                local.insertAll(items)
            }
            //for mocking progress
            val isEndOfPagination = true//((skip + loadSize) >= Constants.maxPageSize)
            return MediatorResult.Success(endOfPaginationReached = isEndOfPagination)
        } catch (e: IOException) {
            return MediatorResult.Error(e)
        } catch (e: HttpException) {
            return MediatorResult.Error(e)
        }
    }

}