package com.gmail.hejosadix.core.data.cats.remote.datasources

import com.gmail.hejosadix.core.data.cats.remote.api.CatsApi

internal class CatsRemoteDataSourceImpl(private val catsApi: CatsApi) : CatsRemoteDataSource {
    override suspend fun fetchCats(skip: Int, limit: Int) = catsApi.fetchCats(
        skip = skip,
        limit = limit
    )

}