package com.gmail.hejosadix.core.data.cats.mappers

import com.gmail.hejosadix.core.database.cat.DbCat
import  com.gmail.hejosadix.core.data.cats.models.Cat

fun DbCat.toCat(imageUrl: String) = Cat(
    id = id,
    owner = owner,
    tags = tags,
    createdAt = createdAt,
    updatedAt = updatedAt,
    imageUrl = imageUrl
)

fun Cat.toDbCat() = DbCat(
    id = id,
    owner = owner,
    tags = tags,
    createdAt = createdAt,
    updatedAt = updatedAt
)