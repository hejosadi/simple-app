package com.gmail.hejosadix.core.data.cats.models

import com.squareup.moshi.Json


data class Cat(
    @Json(name = "_id") val id: String,
    val owner: String?,
    val tags: List<String> = emptyList(),
    val createdAt: String = "",
    val updatedAt: String = "",
    val imageUrl: String = ""
)
