package com.gmail.hejosadix.core.data.cats.remote.datasources

import com.gmail.hejosadix.core.data.cats.models.Cat

interface CatsRemoteDataSource {
    suspend fun fetchCats(
        skip: Int,
        limit: Int,
    ): List<Cat>
}