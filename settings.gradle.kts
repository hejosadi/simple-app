pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "Simple App"
include(":app")
include(":core")
include(":core:network")
include(":data")
include(":data:cats")
include(":features:cats")
include(":core:ui")
include(":core:database")
