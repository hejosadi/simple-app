object Versions {
    const val core = "1.12.0"
    const val appCompat = "1.6.1"
    const val material = "1.11.0"
    const val lifecycle = "2.7.0"
    const val coroutines = "1.6.4"
    const val compose = "1.2.1"
    const val paging = "3.2.1"
    const val composePaging = "3.3.0-alpha03"
    const val retrofit = "2.9.0"
    const val moshi = "1.14.0"
    const val room = "2.6.1"
    const val composeNavigation = "2.7.7"
    const val koin = "3.5.3"
    const val coilCompose = "2.6.0"
    const val junit = "4.13.2"
    const val junitExt = "1.1.5"
    const val coroutinesTest = "1.7.3"
}

object Libs {

    const val core = "androidx.core:core-ktx:${Versions.core}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val lifecycle = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycle}"

    // Compose
    const val composeBom = "androidx.compose:compose-bom:2023.08.00"
    const val composeUI = "androidx.compose.ui:ui"
    const val composeActivity = "androidx.activity:activity-compose:1.8.2"
    const val composeUIGraphics = "androidx.compose.ui:ui-graphics"
    const val composePreview = "androidx.compose.ui:ui-tooling-preview"
    const val composeUiTooling = "androidx.compose.ui:ui-tooling"
    const val composeMaterial = "androidx.compose.material3:material3"
    const val composeNavigation =
        "androidx.navigation:navigation-compose:${Versions.composeNavigation}"

    // paging
    const val paging = "androidx.paging:paging-runtime-ktx:${Versions.paging}"
    const val composePaging = "androidx.paging:paging-compose:${Versions.composePaging}"
    const val pagingTest = "androidx.paging:paging-common:${Versions.paging}"
    const val pagingTesting = "androidx.paging:paging-testing:${Versions.paging}"

    // Koin
    const val koin = "io.insert-koin:koin-android:${Versions.koin}"
    const val koinCompose = "io.insert-koin:koin-androidx-compose:${Versions.koin}"
    const val koinTest = "io.insert-koin:koin-test:${Versions.koin}"
    const val koinTestJUnit4 = "io.insert-koin:koin-test-junit4:${Versions.koin}"

    // Coil
    const val coil = "io.coil-kt:coil-compose:${Versions.coilCompose}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val moshi = "com.squareup.moshi:moshi-kotlin:${Versions.moshi}"
    const val moshiAdapters = "com.squareup.moshi:moshi-adapters:${Versions.moshi}"
    const val retrofitMoshi = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"

    // Room
    const val room = "androidx.room:room-runtime:${Versions.room}"
    const val roomKtx = "androidx.room:room-ktx:${Versions.room}"
    const val roomCompiler = "androidx.room:room-compiler:${Versions.room}"
    const val roomPaging = "androidx.room:room-paging:${Versions.room}"
    const val roomTesting = "androidx.room:room-testing:${Versions.room}"


    const val junit = "junit:junit:${Versions.junit}"
    const val composeUiTest = "androidx.compose.ui:ui-test-junit4:${Versions.compose}"
    const val junitExt = "androidx.test.ext:junit:${Versions.junitExt}"
    const val mockito = "org.mockito:mockito-core:3.12.4"
    const val mockitoKotlin = "org.mockito.kotlin:mockito-kotlin:3.2.0"
    const val composeUITestManifest = "androidx.compose.ui:ui-test-manifest:1.6.3"
    const val coroutinesTest =
        "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutinesTest}"
    const val navigationTesting =
        "androidx.navigation:navigation-testing:${Versions.composeNavigation}"
    const val coreTesting = "android.arch.core:core-testing:1.0.0"
    const val testRunner = "androidx.test:runner:1.5.2"


}