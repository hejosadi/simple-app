# Simple app

## Prerequisites
Android studio (https://developer.android.com/studio) latest stable version

## Installation
Clone the repository and open it in Android Studio

## Usage
Run the app in an emulator or a physical device
## Notes
This is a simple app that demonstrates the use of the following:
multi-module project focusing on feature modules
MVVM architecture
Kotlin

pd: If you want to see the project separately by layers, (data, domain, presentation) let me know and I will upload it to the repository
