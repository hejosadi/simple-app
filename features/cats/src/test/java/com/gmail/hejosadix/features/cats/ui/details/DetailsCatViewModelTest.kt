package com.gmail.hejosadix.features.cats.ui.details

import androidx.lifecycle.SavedStateHandle
import com.gmail.hejosadix.core.data.cats.repositories.CatsRepository
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class DetailsCatViewModelTest {
    @Mock
    private lateinit var repository: CatsRepository

    private lateinit var viewModel: DetailsCatViewModel
    private lateinit var savedStateHandle: SavedStateHandle

    private val testId = "2"

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        savedStateHandle = SavedStateHandle(mapOf("id" to testId))
        viewModel = DetailsCatViewModel(repository, savedStateHandle)
    }

    @Test
    fun `uiState emits Loading state initially`() = runTest {
        val stateFlow: StateFlow<DetailsCatState> = viewModel.uiState
        assertEquals(DetailsCatState.Loading, stateFlow.value)

    }

}
