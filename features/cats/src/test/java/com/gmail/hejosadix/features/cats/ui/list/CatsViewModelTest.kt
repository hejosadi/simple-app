package com.gmail.hejosadix.features.cats.ui.list

import androidx.paging.PagingData
import com.gmail.hejosadix.core.data.cats.repositories.CatsRepository
import org.junit.Before
import org.junit.Test
import androidx.paging.testing.asSnapshot
import com.gmail.hejosadix.features.cats.utils.FakeCats
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain

import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class CatsViewModelTest {


    @Mock
    private lateinit var catsRepository: CatsRepository
    private lateinit var viewModel: CatsViewModel

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(Dispatchers.Unconfined)
        viewModel = CatsViewModel(catsRepository)
    }


    @Test
    fun `fetchCats returns expected data`() = runTest {
        val data = FakeCats.generateFakeCats()
        `when`(catsRepository.fetchCats()).thenReturn(
            flowOf(PagingData.from(data))
        )
        val item = catsRepository.fetchCats().asSnapshot()
        assert(item == data)
    }


}