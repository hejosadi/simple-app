package com.gmail.hejosadix.features.cats.ui.list

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import com.gmail.hejosadix.core.data.cats.repositories.CatsRepository
import com.gmail.hejosadix.features.cats.utils.FakeCatsRepository

import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CatsRouteTest {

    @get:Rule
    val composeTestRule = createComposeRule()
    private lateinit var repository: CatsRepository
    private lateinit var viewModel: CatsViewModel

    @Before
    fun setup() {
        repository = FakeCatsRepository()
        viewModel = CatsViewModel(repository)
        composeTestRule.setContent {
            CatsRoute(
                onGoToItem = {},
                viewModel = viewModel,
            )

        }
    }

    @Test
     fun validate_top_bar_title() {
        composeTestRule.onNodeWithText("Cats").assertIsDisplayed()
    }
    @Test
    fun validate_welcome_message() {
        composeTestRule.onNodeWithText("Welcome to the Cats App!").assertIsDisplayed()
        composeTestRule.onNodeWithText("In this app, you can find a list of cats and their tags.").assertIsDisplayed()
    }

}