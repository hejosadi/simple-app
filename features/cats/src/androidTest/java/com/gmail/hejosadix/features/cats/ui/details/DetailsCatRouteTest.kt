package com.gmail.hejosadix.features.cats.ui.details

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.isDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import com.gmail.hejosadix.core.data.cats.repositories.CatsRepository
import androidx.lifecycle.SavedStateHandle
import com.gmail.hejosadix.core.common.removeStringNullToUnknown
import com.gmail.hejosadix.features.cats.utils.FakeCats
import com.gmail.hejosadix.features.cats.utils.FakeCatsRepository
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DetailsCatRouteTest {
    @get:Rule
    val composeTestRule = createComposeRule()
    private lateinit var repository: CatsRepository
    private lateinit var viewModel: DetailsCatViewModel
    private lateinit var savedStateHandle: SavedStateHandle
    private val id = "1"

    @Before
    fun setup() {
        savedStateHandle = SavedStateHandle()
        savedStateHandle["id"] = id
        repository = FakeCatsRepository()

        viewModel = DetailsCatViewModel(repository, savedStateHandle)
    }

    @Test
    fun validate_on_go_back_press() {
        var navigationActionClicked = false
        composeTestRule.setContent {
            DetailsCatRoute(
                onGoBack = {
                    navigationActionClicked = true
                },
                viewModel = viewModel,
            )

        }
        composeTestRule.onNodeWithTag("nav_icon").performClick()
        assert(navigationActionClicked)
    }

    @Test
    fun validate_title() {
        val data = FakeCats.generateFakeCats().first {
            it.id == id
        }

        composeTestRule.setContent {
            DetailsCatRoute(
                onGoBack = {

                },
                viewModel = viewModel,
            )

        }
        composeTestRule.onNodeWithText(data.id).isDisplayed()
    }

    @Test
    fun validate_tags() {
        val data = FakeCats.generateFakeCats().first {
            it.id == id
        }

        composeTestRule.setContent {
            DetailsCatRoute(
                onGoBack = {

                },
                viewModel = viewModel,
            )

        }
        data.tags.forEach {
            composeTestRule.onNodeWithText(it).assertIsDisplayed()
        }
        composeTestRule.onNodeWithText("Tags").assertIsDisplayed()
    }

    @Test
    fun validate_owner() {
        val data = FakeCats.generateFakeCats().first {
            it.id == id
        }

        composeTestRule.setContent {
            DetailsCatRoute(
                onGoBack = {

                },
                viewModel = viewModel,
            )

        }
        composeTestRule.onNodeWithText(
            data.owner.orEmpty()
                .removeStringNullToUnknown(
                    "Unknown"
                )
        ).assertIsDisplayed()
        composeTestRule.onNodeWithTag("owner_icon").assertIsDisplayed()
    }
}