package com.gmail.hejosadix.features.cats.utils

import androidx.paging.PagingData
import com.gmail.hejosadix.core.data.cats.models.Cat
import com.gmail.hejosadix.core.data.cats.repositories.CatsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf

class FakeCatsRepository : CatsRepository {
    private val data = FakeCats.generateFakeCats()
    override fun fetchCats() = flow {
        emit(PagingData.from(data))
    }

    override fun fetchCat(id: String): Flow<Cat> {
        return flowOf(data.first {
            it.id == id
        })
    }

    override fun fetchCatImageUrl(id: String): String {
        return data.first {
            it.id == id
        }.let {
            "https://cataas.com/cat/${it.id}"
        }
    }

}