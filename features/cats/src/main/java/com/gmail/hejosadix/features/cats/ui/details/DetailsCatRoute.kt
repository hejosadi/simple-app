package com.gmail.hejosadix.features.cats.ui.details


import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.gmail.hejosadix.core.common.removeStringNullToUnknown
import com.gmail.hejosadix.core.data.cats.models.Cat
import com.gmail.hejosadix.core.ui.images.ImageShape
import com.gmail.hejosadix.core.R as coreR
import com.gmail.hejosadix.core.ui.cards.TagCard
import com.gmail.hejosadix.core.ui.images.imageRoundedCornerShapeEnd
import com.gmail.hejosadix.core.ui.lists.TagsLazyRow
import org.koin.androidx.compose.koinViewModel
import com.gmail.hejosadix.features.cats.R

@Composable
fun DetailsCatRoute(
    onGoBack: () -> Unit,
    viewModel: DetailsCatViewModel = koinViewModel<DetailsCatViewModel>()
) {
    val state by viewModel.uiState.collectAsState()
    DetailsCatScreen(state = state, onGoBack = onGoBack)
}


@Composable
internal fun DetailsCatScreen(
    state: DetailsCatState,
    onGoBack: () -> Unit,
) {
    when (state) {
        DetailsCatState.Loading -> Unit
        is DetailsCatState.Success -> Content(cat = state.cat, url = state.url, onGoBack = onGoBack)
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun Content(
    cat: Cat?, url: String = "",
    onGoBack: () -> Unit,
) {
    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(
                        modifier = Modifier.testTag("nav_icon"),
                        onClick = onGoBack
                    ) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "Back")

                    }
                },
                title = {},
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = Color.Transparent,
                    navigationIconContentColor = Color.White,
                    actionIconContentColor = Color.White,
                ),
                modifier = Modifier.background(Color.Transparent),
            )
        },

        modifier = Modifier
            .fillMaxSize()
            .background(Color.Transparent),

        ) { paddingValues ->
        val configuration = LocalConfiguration.current
        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            PortraitContent(
                cat = cat, url = url, modifier = Modifier
                    .padding(paddingValues)
            )
        } else {
            LandscapeContent(
                cat = cat, url = url,
            )
        }

    }
}

@Composable
fun PortraitContent(modifier: Modifier = Modifier, cat: Cat?, url: String = "") {
    Column(modifier = Modifier) {
        ImageShape(url = url, modifier = Modifier.fillMaxWidth())
        cat?.let {
            Info(modifier = modifier, cat = cat)
        }

    }
}

@Composable
fun LandscapeContent(cat: Cat?, url: String = "") {
    Row(modifier = Modifier) {
        ImageShape(
            url = url, modifier = Modifier
                .fillMaxHeight()
                .widthIn(max = dimensionResource(id = coreR.dimen.image_width)),
            shape = imageRoundedCornerShapeEnd()
        )
        cat?.let {
            Info(modifier = Modifier.padding(top = 5.dp), cat = cat)
        }

    }
}

@Composable
private fun Info(modifier: Modifier = Modifier, cat: Cat) {
    LazyColumn(
        modifier = modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.End,
    ) {
        item {
            IDSection(cat = cat)
        }
        item {
            OwnerInfo(
                cat = cat,
                modifier = Modifier.padding(vertical = dimensionResource(id = coreR.dimen.default_padding))
            )
        }
        item {
            TagsSection(cat = cat)
        }
    }
}

@Composable
private fun OwnerInfo(
    modifier: Modifier = Modifier,
    cat: Cat
) {
    Box(
        modifier = modifier
            .fillMaxWidth()
            .padding(dimensionResource(id = coreR.dimen.default_padding))
            .clip(RoundedCornerShape(dimensionResource(id = coreR.dimen.default_padding))),

        ) {
        Row(
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.secondaryContainer)
                .padding(dimensionResource(id = coreR.dimen.default_padding))
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                modifier = modifier.testTag("owner_icon")
                    .size(
                        dimensionResource(id = coreR.dimen.icon_size)
                    ),
                imageVector = Icons.Default.AccountCircle, contentDescription = null,
            )

            Text(
                modifier = Modifier
                    .padding(
                        horizontal = dimensionResource(id = coreR.dimen.default_padding)
                    ),
                text = stringResource(id = R.string.owner),
                style = MaterialTheme.typography.titleLarge
            )
            Text(
                text = cat.owner.orEmpty().removeStringNullToUnknown(
                    stringResource(id = R.string.unknown_owner)
                ),
                style = MaterialTheme.typography.titleMedium
            )


        }

    }
}

@Composable
private fun IDSection(
    modifier: Modifier = Modifier,
    cat: Cat
) {
    Box(
        modifier = modifier
            .clip(
                RoundedCornerShape(
                    topStart = dimensionResource(id = coreR.dimen.default_padding),
                    bottomStart = dimensionResource(id = coreR.dimen.default_padding),
                    bottomEnd = 0.dp,
                    topEnd = 0.dp
                )
            ),

        ) {
        Row(
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.onBackground)


        ) {
            Text(
                modifier = Modifier
                    .background(color = MaterialTheme.colorScheme.surfaceVariant)
                    .fillMaxHeight()
                    .padding(
                        dimensionResource(id = coreR.dimen.default_padding)
                    ),
                text = stringResource(id = R.string.id),
                style = MaterialTheme.typography.titleLarge
            )
            Text(
                modifier = Modifier.padding(
                    dimensionResource(id = coreR.dimen.default_padding)
                ),
                text = cat.id,
                style = MaterialTheme.typography.titleLarge.copy(
                    color = MaterialTheme.colorScheme.surfaceVariant
                )
            )
        }

    }
}

@Composable
private fun TagsSection(
    modifier: Modifier = Modifier,
    cat: Cat
) {
    Row(
        modifier = modifier
            .padding(
                start = dimensionResource(id = coreR.dimen.default_padding)
            ),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = stringResource(id = R.string.tags)
        )
        Icon(imageVector = Icons.Default.KeyboardArrowRight, contentDescription = null)
        TagsLazyRow(
            items = cat.tags,
            modifier = Modifier
                .testTag("cat_list"),
            emptyContent = {
                TagCard(
                    title = stringResource(id = R.string.no_tags)
                )
            }
        ) {
            TagCard(title = it)
        }
    }

}

