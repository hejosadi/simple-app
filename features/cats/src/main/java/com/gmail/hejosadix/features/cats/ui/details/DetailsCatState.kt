package com.gmail.hejosadix.features.cats.ui.details

import com.gmail.hejosadix.core.data.cats.models.Cat

sealed interface DetailsCatState {
    data object Loading : DetailsCatState
    data class Success(
        val cat: Cat? = null,
        val url: String = "",
    ) : DetailsCatState
}