package com.gmail.hejosadix.features.cats.ui.details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gmail.hejosadix.core.data.cats.repositories.CatsRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn

class DetailsCatViewModel(
    private val repository: CatsRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    @OptIn(ExperimentalCoroutinesApi::class)
    val uiState: StateFlow<DetailsCatState> = savedStateHandle
        .getStateFlow<String?>("id", null)
        .filterNotNull()
        .flatMapLatest { id ->
            repository.fetchCat(id)
        }.map { model ->
            DetailsCatState.Success(
                cat = model,
                url = repository.fetchCatImageUrl(model.id)
            )
        }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), DetailsCatState.Loading)
}