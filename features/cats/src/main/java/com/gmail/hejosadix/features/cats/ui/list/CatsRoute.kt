package com.gmail.hejosadix.features.cats.ui.list


import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import com.gmail.hejosadix.core.common.removeStringNullToUnknown
import com.gmail.hejosadix.core.data.cats.models.Cat
import com.gmail.hejosadix.core.ui.lists.CatItem
import com.gmail.hejosadix.core.ui.lists.PrimaryLazyColumn
import com.gmail.hejosadix.core.ui.progress_indicators.FullScreenCircularIndeterminateProgressBar
import com.gmail.hejosadix.features.cats.R
import org.koin.androidx.compose.koinViewModel
import com.gmail.hejosadix.core.R as CoreR

@Composable
fun CatsRoute(
    onGoToItem: (String) -> Unit,
    modifier: Modifier = Modifier,
    viewModel: CatsViewModel = koinViewModel<CatsViewModel>()
) {
    CatsScreen(viewModel = viewModel, onGoToItem = onGoToItem, modifier = modifier)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
internal fun CatsScreen(
    viewModel: CatsViewModel,
    onGoToItem: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    val snackBarHostState = remember { SnackbarHostState() }
    val cats = viewModel.cats.collectAsLazyPagingItems()
    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackBarHostState) },
        topBar = {
            CenterAlignedTopAppBar(
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(),
                title = {
                    Text(
                        text = stringResource(id = R.string.cats),
                    )
                },
                scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(),
            )
        },

        modifier = modifier
    ) { paddingValues ->
        cats.loadState.ShowMessage(snackBarHostState = snackBarHostState)
        Content(modifier = Modifier.padding(paddingValues), cats = cats, onGoToItem = onGoToItem)
    }
}

@Composable
private fun Content(
    modifier: Modifier = Modifier,
    cats: LazyPagingItems<Cat>, onGoToItem: (String) -> Unit = {}
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .fillMaxSize()
    ) {
        Column(modifier = Modifier.widthIn(max = dimensionResource(id = CoreR.dimen.screen_width))) {
            Welcome()
            if (cats.loadState.refresh is LoadState.Loading) {
                FullScreenCircularIndeterminateProgressBar()
            } else {
                val range = (0 until cats.itemCount).toList()
                PrimaryLazyColumn(
                    items = range,
                    modifier = Modifier
                        .testTag("cat_list"),
                )
                {
                    cats[it]?.let { cat ->
                        CatItem(
                            id = cat.id,
                            title = "${stringResource(id = R.string.owner)} ${
                                cat.owner.orEmpty().removeStringNullToUnknown(
                                    stringResource(id = R.string.unknown_owner)
                                )
                            }",
                            imageUrl = cat.imageUrl,
                            onGoToItem = onGoToItem,
                            description = cat.tags.joinToString(", ")
                        )
                    }

                }
            }
        }
    }

}

@Composable
fun Welcome(
) {
    Column(
        modifier = Modifier
            .padding(dimensionResource(id = CoreR.dimen.default_padding)),
    ) {
        Text(
            text = stringResource(id = R.string.welcome),

            )
        Text(
            text = stringResource(id = R.string.welcome_details),

            style = MaterialTheme.typography.bodyMedium.copy(
                color = MaterialTheme.colorScheme.surfaceVariant
            ),
        )
    }
}

@Composable
private fun CombinedLoadStates.ShowMessage(snackBarHostState: SnackbarHostState) {
    if (refresh is LoadState.Error) {
        LaunchedEffect(key1 = snackBarHostState) {
            snackBarHostState.showSnackbar(
                (refresh as LoadState.Error).error.message ?: ""
            )
        }
    }
}

