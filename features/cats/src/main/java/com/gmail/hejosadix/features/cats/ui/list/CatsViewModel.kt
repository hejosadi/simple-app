package com.gmail.hejosadix.features.cats.ui.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.gmail.hejosadix.core.data.cats.models.Cat
import com.gmail.hejosadix.core.data.cats.repositories.CatsRepository
import kotlinx.coroutines.flow.Flow

class CatsViewModel (
    catsRepository: CatsRepository
) : ViewModel() {


    val cats: Flow<PagingData<Cat>> = catsRepository.fetchCats().cachedIn(viewModelScope)

}