package com.gmail.hejosadix.features.cats.di

import com.gmail.hejosadix.features.cats.ui.details.DetailsCatViewModel
import com.gmail.hejosadix.features.cats.ui.list.CatsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val featuresCatsModule = module {
    viewModel { CatsViewModel(get()) }
    viewModel { DetailsCatViewModel(get(), get()) }
}