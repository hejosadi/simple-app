plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.gmail.hejosadix.features.cats"
    compileSdk = 34

    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.1"
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    testOptions {
        unitTests.isReturnDefaultValues = true
    }
}

dependencies {
    implementation(project(":core"))
    implementation(project(":core:ui"))
    implementation(project(":data:cats"))
    // Core
    implementation(Libs.core)
    implementation(Libs.appCompat)
    implementation(Libs.material)
    implementation(Libs.lifecycle)

    //compose
    implementation(Libs.composeActivity)
    implementation(Libs.composeUI)
    implementation(Libs.composeMaterial)
    implementation(platform(Libs.composeBom))
    implementation(Libs.composeUIGraphics)
    //paging
    implementation(Libs.paging)
    implementation(Libs.composePaging)
    //koin
    implementation(Libs.koin)
    implementation(Libs.koinCompose)
    //Test
    testImplementation(Libs.junit)
    testImplementation(Libs.pagingTest)
    testImplementation(Libs.pagingTesting)
    testImplementation(Libs.coroutinesTest)
    testImplementation(Libs.mockito)
    testImplementation(Libs.mockitoKotlin)
    androidTestImplementation(Libs.junitExt)
    androidTestImplementation(Libs.junit)
    androidTestImplementation(Libs.pagingTesting)
    androidTestImplementation(Libs.coroutinesTest)
    androidTestImplementation(Libs.mockito)
    androidTestImplementation(Libs.mockitoKotlin)
    androidTestImplementation(Libs.composeUiTest)
    debugImplementation(Libs.composeUITestManifest)


}