plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("androidx.room")
    id("kotlin-kapt")
}

android {
    namespace = "com.gmail.hejosadix.core.database"
    compileSdk = 34
    room {
        schemaDirectory("$projectDir/schemas")
    }
    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(project(":core"))
    //koin
    implementation(Libs.koin)
    // Room
    implementation(Libs.room)
    implementation(Libs.roomKtx)
    implementation(Libs.testRunner)
    annotationProcessor(Libs.roomCompiler)
    kapt(Libs.roomCompiler)
    implementation(Libs.roomPaging)

    //moshi
    implementation(Libs.moshi)
    implementation(Libs.moshiAdapters)
    //test
    androidTestImplementation(Libs.junitExt)
    androidTestImplementation(Libs.junit)
    androidTestImplementation(Libs.pagingTesting)
    androidTestImplementation(Libs.coroutinesTest)
    androidTestImplementation(Libs.mockito)
    androidTestImplementation(Libs.mockitoKotlin)
    androidTestImplementation(Libs.coreTesting)
    androidTestImplementation(Libs.roomTesting)
    androidTestImplementation(Libs.koinTest)
    androidTestImplementation(Libs.koinTestJUnit4)


}