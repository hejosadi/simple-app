package com.gmail.hejosadix.core.database.cat

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.gmail.hejosadix.core.database.AppDatabase
import com.gmail.hejosadix.core.di.coreModule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.KoinTestRule
import java.util.concurrent.CountDownLatch

@RunWith(AndroidJUnit4::class)
@SmallTest
class CatDaoTest {
    private lateinit var database: AppDatabase
    private lateinit var catDao: CatDao

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        printLogger()
        modules(coreModule)
    }

    @Before
    fun setupDatabase() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).build()

        catDao = database.catDao()
    }

    @Test
    fun insert_cat_returns_true() = runBlocking {
        val id = "123"
        val cats = DbCat(id, "url", listOf())
        catDao.insert(cats)
        val latch = CountDownLatch(1)
        val job = async(Dispatchers.IO) {

            catDao.getCat(id).collectLatest {
                assert(it.id == id)
                latch.countDown()
            }
        }
        latch.await()
        job.cancelAndJoin()
    }

    @Test
    fun delete_returns_true() = runBlocking {
        val cats = (1 until 2).map {
            DbCat(it.toString(), "url", listOf())
        }
        catDao.insertAll(cats)
        catDao.clearCache()

        assert(catDao.count() == 0)
    }

    @Test
    fun insert_and_clear_cache_returns_true() = runBlocking {
        val cats = (1 until 10).map {
            DbCat(it.toString(), "url", listOf())
        }
        catDao.insertAndClearCache(cats)
        val latch = CountDownLatch(1)
        val job = async(Dispatchers.IO) {

            catDao.getAllCats().collectLatest {
                assert(cats == it)
                latch.countDown()
            }
        }
        latch.await()
        job.cancelAndJoin()

    }

    @After
    fun closeDatabase() {
        database.close()
    }
}