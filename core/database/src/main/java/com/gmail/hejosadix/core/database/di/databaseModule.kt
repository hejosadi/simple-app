package com.gmail.hejosadix.core.database.di

import com.gmail.hejosadix.core.database.AppDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


val databaseModule = module {
    factory<AppDatabase> {
        AppDatabase.getDatabase(androidApplication())
    }
    single {
        val appDatabase: AppDatabase = get()
        appDatabase.catDao()
    }


}