package com.gmail.hejosadix.core.database.cat

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cats")
data class DbCat(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val owner: String?,
    val tags: List<String> = emptyList(),
    val createdAt: String = "",
    val updatedAt: String = "",
)
