package com.gmail.hejosadix.core.database.cat

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import kotlinx.coroutines.flow.Flow


@Dao
interface CatDao {

    @Query("SELECT * FROM cats")
    fun getAllCats(): Flow<List<DbCat>>

    @Query("SELECT * FROM cats WHERE id = :id LIMIT 1")
    fun getCat(id: String): Flow<DbCat>

    @Query("SELECT * FROM cats")
    fun getAllCatPagingSource(): PagingSource<Int, DbCat>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: DbCat): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<DbCat>)


    @Query("DELETE FROM cats")
    suspend fun clearCache()

    @Query("SELECT COUNT(*) FROM cats")
    suspend fun count(): Int

    @Transaction
    suspend fun insertAndClearCache(items: List<DbCat>) {
        clearCache()
        insertAll(items)
    }


}