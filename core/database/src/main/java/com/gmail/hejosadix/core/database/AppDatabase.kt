package com.gmail.hejosadix.core.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.gmail.hejosadix.core.database.cat.CatDao
import com.gmail.hejosadix.core.database.cat.DbCat
import com.gmail.hejosadix.core.database.utils.TypeConverter

@Database(
    entities = [DbCat::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(
    value = [TypeConverter::class]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun catDao(): CatDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null
        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "db"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}