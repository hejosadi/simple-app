package com.gmail.hejosadix.core.common


fun String.removeStringNullToUnknown(
    textToReplace: String
): String {
    return if (this == "null") textToReplace else this
}