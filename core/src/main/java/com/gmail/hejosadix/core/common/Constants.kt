package com.gmail.hejosadix.core.common

object Constants {
    const val defaultPageSize = 10
    const val maxPage = 10
    const val maxPageSize = maxPage * defaultPageSize
}