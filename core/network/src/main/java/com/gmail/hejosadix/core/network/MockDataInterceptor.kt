package com.gmail.hejosadix.core.network

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Protocol
import okhttp3.Response

import java.io.IOException

class MockDataInterceptor(private val context: Context) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val endpoint = originalRequest.url().encodedPath()
        if (!endpoint.contains("api/cats")) {
            return chain.proceed(originalRequest)
        }

        println("Intercepting request to $endpoint and returning mock data.")
        try {
            val inputStream = context.assets.open("data/mock_response.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()

            val json = String(buffer)
            val mediaType = okhttp3.MediaType.parse("application/json")
            val responseBody = okhttp3.ResponseBody.create(mediaType, json)

            return Response.Builder()
                .code(200)
                .message("OK")
                .protocol(Protocol.HTTP_2)
                .request(originalRequest)
                .body(responseBody)
                .build()
        } catch (e: IOException) {
            e.printStackTrace()
            return Response.Builder()
                .code(500)
                .message(e.message ?: "Internal Server Error")
                .protocol(Protocol.HTTP_2)
                .request(originalRequest)
                .build()
        }
    }


}