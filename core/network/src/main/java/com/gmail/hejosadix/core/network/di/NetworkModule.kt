package com.gmail.hejosadix.core.network.di

import com.gmail.hejosadix.core.network.BuildConfig
import com.gmail.hejosadix.core.network.MockDataInterceptor
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val networkModule = module {

    single {
        OkHttpClient.Builder().addInterceptor(
            MockDataInterceptor(
                androidApplication()
            )
        ).build()
    }
    single { retrofit(get(), get()) }
}


private fun retrofit(client: OkHttpClient, moshi: Moshi) = Retrofit.Builder()
    .baseUrl(BuildConfig.BASE_URL)
    .client(client)
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .build()