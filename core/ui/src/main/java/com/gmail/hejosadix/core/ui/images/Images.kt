package com.gmail.hejosadix.core.ui.images

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.gmail.hejosadix.core.R

@Composable
fun ImageShape(
    modifier: Modifier = Modifier,
    url: String,
    shape: Shape =
        imageRoundedCornerShapeBottom(),
    height: Dp = dimensionResource(
        id = R.dimen.image_height
    ),
) {
    var showProgressIndicator by remember { mutableStateOf(true) }
    val color =
        if (showProgressIndicator) Color.Transparent else MaterialTheme.colorScheme.surfaceVariant
    Box(
        modifier = modifier
            .height(
                height
            )
            .clip(
                shape = shape
            ),
        contentAlignment = Alignment.Center
    ) {
        if (showProgressIndicator) {

            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = MaterialTheme.colorScheme.surfaceVariant),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator()
            }


        }
        AsyncImage(
            modifier = Modifier
                .fillMaxSize()
                .background(color = color),
            model = url,
            contentScale = ContentScale.FillBounds,
            contentDescription = null,
            onError = { error ->
                println("AsyncImage Error loading image $url ${error.result.throwable.message}")
                showProgressIndicator = false
            }
        )
    }

}

@Composable
fun ImageSquareRounded(
    modifier: Modifier = Modifier,
    url: String,
    shape: Shape =
        RoundedCornerShape(
            dimensionResource(id = R.dimen.item_rounded_size)
        ),

    ) {
    var showProgressIndicator by remember { mutableStateOf(true) }
    val color =
        if (showProgressIndicator) Color.Transparent else MaterialTheme.colorScheme.surfaceVariant
    Box(
        modifier = modifier
            .clip(
                shape = shape
            )
    ) {
        if (showProgressIndicator) {
            CircularProgressIndicator(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = MaterialTheme.colorScheme.surfaceVariant)
                    .padding(dimensionResource(id = R.dimen.small_padding))
            )
        }
        AsyncImage(
            modifier = Modifier.background(color = color),
            model = url,
            contentScale = ContentScale.Crop,
            contentDescription = null,
            onError = { error ->
                showProgressIndicator = false
            }
        )

    }
}

@Composable
fun imageRoundedCornerShapeBottom() = RoundedCornerShape(
    0.dp,
    0.dp,
    dimensionResource(id = R.dimen.primary_image_rounded_size),
    dimensionResource(id = R.dimen.primary_image_rounded_size),
)

@Composable
fun imageRoundedCornerShapeEnd() = RoundedCornerShape(
    0.dp,
    dimensionResource(id = R.dimen.primary_image_rounded_size),
    dimensionResource(id = R.dimen.primary_image_rounded_size),
    0.dp,
)
