package com.gmail.hejosadix.core.ui.lists

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.dp
import com.gmail.hejosadix.core.R


@Composable
fun <T> PrimaryLazyColumn(
    items: List<T>,
    modifier: Modifier = Modifier,
    itemContent: @Composable (item: T) -> Unit
) {
    LazyColumn(
        modifier = modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .clip(
                RoundedCornerShape(
                    dimensionResource(id = R.dimen.primary_column_rounded_size),
                    dimensionResource(id = R.dimen.primary_column_rounded_size),
                    0.dp,
                    0.dp,
                )
            )
            .background(color = MaterialTheme.colorScheme.surfaceVariant)
            .padding(horizontal = dimensionResource(id = R.dimen.default_padding)),
        horizontalAlignment = Alignment.CenterHorizontally,
        contentPadding = PaddingValues(vertical = 50.dp),
        verticalArrangement = Arrangement.spacedBy(dimensionResource(id = R.dimen.default_padding)),
    ) {
        items(items) { item ->
            itemContent(item)
        }
    }
}

@Composable
fun <T> TagsLazyRow(
    items: List<T>,
    modifier: Modifier = Modifier,
    emptyContent: @Composable () -> Unit = {},
    itemContent: @Composable (item: T) -> Unit,
) {
    Box(
        modifier = modifier
            .padding(top = 8.dp)
            .fillMaxWidth()
            .clip(
                RoundedCornerShape(
                    topStart = dimensionResource(id = R.dimen.default_padding),
                    topEnd = 0.dp,
                    bottomStart = dimensionResource(id = R.dimen.default_padding),
                    bottomEnd = 0.dp,
                )
            )
            .background(color = MaterialTheme.colorScheme.surfaceVariant)
    ) {
        LazyRow(
            modifier = Modifier
                .padding(vertical = dimensionResource(id = R.dimen.default_padding))
                .padding(
                    start = dimensionResource(id = R.dimen.default_padding)
                ),
            horizontalArrangement = Arrangement.spacedBy(dimensionResource(id = R.dimen.small_padding))
        ) {
            items(items) { item ->
                itemContent(item)
            }
            if (items.isEmpty()) {
                item {
                    emptyContent()
                }
            }
        }
    }
}
