package com.gmail.hejosadix.core.ui.lists

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gmail.hejosadix.core.R as coreR
import com.gmail.hejosadix.core.ui.images.ImageSquareRounded

@Composable
fun CatItem(
    modifier: Modifier = Modifier,
    id: String,
    title: String,
    description: String,
    onGoToItem: (String) -> Unit = {},
    imageUrl: String = ""
) {
    Row(
        modifier = modifier
            .clickable {
                onGoToItem(id)
            }
            .clip(RoundedCornerShape(8.dp))
            .background(color = MaterialTheme.colorScheme.background),
        verticalAlignment = Alignment.CenterVertically
    ) {
        ImageSquareRounded(
            modifier = Modifier
                .size(70.dp)
                .padding(dimensionResource(id = coreR.dimen.default_padding)),
            url = imageUrl,
        )
        Column(
            modifier = Modifier
                .weight(1f)
                .align(Alignment.CenterVertically)
        ) {
            Text(
                text = title,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,

                )
            Text(
                text = description,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.bodyMedium.copy(
                    color = MaterialTheme.colorScheme.surfaceVariant
                ),
            )
        }

        Icon(
            modifier = Modifier.padding(end = dimensionResource(id = coreR.dimen.default_padding)),
            imageVector = Icons.Default.KeyboardArrowRight, contentDescription = null
        )
    }

}


@Preview
@Composable
fun Preview() {
    Column {
        CatItem(id = "1", title = "Title", description = "Description")
    }

}