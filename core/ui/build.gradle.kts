plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.gmail.hejosadix.core.ui"
    compileSdk = 34

    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.1"
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    // modules
    implementation(project(":core"))
    // compose
    implementation(Libs.composeActivity)
    implementation(Libs.composeUI)
    implementation(Libs.composeMaterial)
    implementation(platform(Libs.composeBom))
    implementation(Libs.composeUIGraphics)
    implementation(Libs.composePreview)
    debugImplementation(Libs.composeUiTooling)

    //coil
    implementation(Libs.coil)
}