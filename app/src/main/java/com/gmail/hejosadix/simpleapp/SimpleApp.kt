package com.gmail.hejosadix.simpleapp

import android.app.Application
import com.gmail.hejosadix.core.data.cats.di.dataCatsModule
import com.gmail.hejosadix.core.di.coreModule
import com.gmail.hejosadix.core.database.di.databaseModule
import com.gmail.hejosadix.core.network.di.networkModule
import com.gmail.hejosadix.features.cats.di.featuresCatsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class SimpleApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@SimpleApp)
            modules(
                coreModule,
                networkModule,
                databaseModule,
                dataCatsModule,
                featuresCatsModule,
            )
        }
    }
}