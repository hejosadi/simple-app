package com.gmail.hejosadix.simpleapp.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.gmail.hejosadix.features.cats.ui.details.DetailsCatRoute
import com.gmail.hejosadix.features.cats.ui.list.CatsRoute


@Composable
internal fun MainNavigation(
    navController: NavHostController = rememberNavController()
) {
    NavHost(navController = navController, startDestination = "cats") {
        composable("cats") {
            CatsRoute(
                onGoToItem = { id ->
                    navController.navigate("details/$id")
                }
            )
        }

        composable(
            "details/{id}",
            listOf(navArgument("id") { type = NavType.StringType })
        ) {
            DetailsCatRoute(onGoBack = {
                navController.popBackStack()
            })

        }
    }
}