package com.gmail.hejosadix.simpleapp.ui.theme

import android.app.Activity
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat

private val DarkColorScheme = darkColorScheme(
    background = Purple80,
    onBackground = White80,
    onSurface = White80,
    onSurfaceVariant = White80,
    surface = Purple80,
    tertiary = PowderBlue,
    surfaceVariant = Purple20,
    secondaryContainer = Yellow80,
    primaryContainer = Purple60,
    tertiaryContainer = Purple40
)

private val LightColorScheme = lightColorScheme(
    background = Purple80,
    onBackground = White80,
    onSurface = White80,
    onSurfaceVariant = White80,
    surface = Purple80,
    tertiary = PowderBlue,
    surfaceVariant = Purple20,
    secondaryContainer = Yellow80,
    primaryContainer = Purple60,
    tertiaryContainer = Purple40
)

@RequiresApi(Build.VERSION_CODES.S)
private fun simpleAppLightColorScheme(context: Context): ColorScheme {
    return dynamicLightColorScheme(context).copy(
        background = Purple80,
        onBackground = White80,
        onSurface = White80,
        onSurfaceVariant = White80,
        surface = Purple80,
        tertiary = PowderBlue,
        surfaceVariant = Purple20,
        secondaryContainer = Yellow80,
        primaryContainer = Purple60,
        tertiaryContainer = Purple40
    )
}

@RequiresApi(Build.VERSION_CODES.S)
private fun simpleAppDarkColorScheme(context: Context): ColorScheme {
    return dynamicLightColorScheme(context).copy(
        background = Purple80,
        onBackground = White80,
        onSurface = White80,
        onSurfaceVariant = White80,
        surface = Purple80,
        tertiary = PowderBlue,
        surfaceVariant = Purple20,
        secondaryContainer = Yellow80,
        primaryContainer = Purple60,
        tertiaryContainer = Purple40
    )
}

@Composable
fun SimpleAppTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    // Dynamic color is available on Android 12+
    dynamicColor: Boolean = true,
    content: @Composable () -> Unit
) {
    val colorScheme = when {
        dynamicColor && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
            val context = LocalContext.current
            if (darkTheme) simpleAppDarkColorScheme(context) else simpleAppLightColorScheme(context)
        }

        darkTheme -> DarkColorScheme
        else -> LightColorScheme
    }
    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = colorScheme.primary.toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = darkTheme
        }
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content
    )
}