package com.gmail.hejosadix.simpleapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFF54389E)
val Purple60 = Color(0xFF8191F8)
val Purple40 = Color(0xFF939EFA)
val Purple20 = Color(0xFF8E61F5)
val White80 = Color(0xFFFFFFFF)
val Yellow80 = Color(0xFFE5A66B)

val PowderBlue = Color(0xFFAEC5EB)