package com.gmail.hejosadix.simpleapp.navigation

import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.isDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performScrollToIndex
import androidx.navigation.compose.ComposeNavigator
import androidx.navigation.testing.TestNavHostController
import com.gmail.hejosadix.simpleapp.ui.navigation.MainNavigation
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class NavigationTest {
    @get:Rule
    val composeTestRule = createComposeRule()
    private lateinit var navController: TestNavHostController

    @Before
    fun setupAppNavHost() {
        composeTestRule.setContent {
            navController = TestNavHostController(LocalContext.current)
            navController.navigatorProvider.addNavigator(ComposeNavigator())
            MainNavigation(navController = navController)
        }
    }

    @Test
    fun navigate_list_cats() {
        composeTestRule.onNodeWithText("Cats").assertIsDisplayed()
        Thread.sleep(1000)
        composeTestRule.onNodeWithTag("cat_list").assertIsDisplayed()
    }

    @Test
    fun navigate_to_details() {
        Thread.sleep(1000)
        composeTestRule.onNodeWithTag("cat_list").performScrollToIndex(1).performClick()
        val route = navController.currentBackStackEntry?.destination?.route
        composeTestRule.onNodeWithTag("title").isDisplayed()
        assert(route == "details/{id}")
    }
}