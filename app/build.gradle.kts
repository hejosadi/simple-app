plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.gmail.hejosadix.simpleapp"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.gmail.hejosadix.simpleapp"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.1"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {
    // Modules
    implementation(project(":core"))
    implementation(project(":core:network"))
    implementation(project(":core:database"))
    implementation(project(":data:cats"))
    implementation(project(":features:cats"))
    // Core
    implementation(Libs.core)
    implementation(Libs.appCompat)
    implementation(Libs.material)
    implementation(Libs.lifecycle)
    // Compose
    implementation(platform(Libs.composeBom))
    implementation(Libs.composeActivity)
    implementation(Libs.composeUI)
    implementation(Libs.composePreview)
    implementation(Libs.composeUIGraphics)
    implementation(Libs.composeMaterial)
    implementation(Libs.composeNavigation)
    // Koin
    implementation(Libs.koin)
    implementation(Libs.koinCompose)
    // Test

    androidTestImplementation(Libs.junitExt)
    androidTestImplementation(Libs.junit)
    androidTestImplementation(Libs.pagingTesting)
    androidTestImplementation(Libs.coroutinesTest)
    androidTestImplementation(Libs.mockito)
    androidTestImplementation(Libs.mockitoKotlin)
    androidTestImplementation(Libs.composeUiTest)
    debugImplementation(Libs.composeUITestManifest)
    androidTestImplementation(Libs.navigationTesting)
}